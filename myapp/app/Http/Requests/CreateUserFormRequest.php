<?php

namespace app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:30',
            'lastname' => 'required|max:30',
            'email' => 'required|max:50',
            'telephone' => 'required|integer',
            'password' => 'required|min:6',
            'username' => 'required',
            'title' => 'required',
            'usertype_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
          'telephone.integer' =>'Not a telephone number - try again.',
          'password.min' => 'Password must be at least 6 characters long.',
        ];
    }

}
