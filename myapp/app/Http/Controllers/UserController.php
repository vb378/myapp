<?php

namespace App\Http\Controllers;

//use \App\Http\Models\Appuser;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use \App\Http\Requests\CreateUserFormRequest as FormRequest;
Use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Translation\Loader;
//use \Config\session as Session;
use Illuminate\Support\Facades\Session;
use \Input;
use App\Http\Models\Usertype;
use App\Http\Models\Appuser;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$appuser = new \App\Http\Models\Appuser;
        $users = \App\Http\Models\Appuser::all();
        return view('usersview', ['allUsers' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo 'test';
       return view('/');
    }
    public function showAdmin()
    {   session_start();
        if (!isset($_SESSION['user'])) {
        
        return redirect('login')->withErrors('You are not logged in- oops. Please log in.');}
        elseif ($_SESSION['usertype'] == '2') {
        return redirect('allusers')->withErrors('You are not authorised to see the admin page!');
        }
        elseif ($_SESSION['usertype'] == '1') {
        return view('admin');
        }
        else {
         return view('notauthorised');
        }


}
    
    /**
     * Store a newly created resource in storage.     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormRequest $request)
    {
        //return in_array($usertype_id, $selected) ? 'selected' : null;
        \App\Http\Models\Appuser::create([
          'firstname' => $request->get('firstname'),
          'lastname' => $request->get('lastname'),
          'title' => $request->get('title'),
          'email' => $request->get('email'),
          'telephone' => $request->get('telephone'),
          'username' => $request->get('username'),
          'password' => bcrypt($request->get('password')),
          'usertype_id' => $request->get('usertype_id')
           ]);

        return view('usersview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         echo 'test';
         // idk yeet
    }

    public function processAdmin(Request $request)
    {
        session_start();
        $username = $request->get('username');
    $getUser = DB::table('user')->where('username', $username)->first();
    if ($getUser === null) {
        return redirect('admin')->withErrors('This username was not found. Try again?');


   // user doesn't exist
} else {
    DB::update('update user set usertype_id = 1 where username = ?', [$username]);
 return redirect('admin')->withErrors('User successfully updated to admin status.');
}
        
        }
   // user doesn't exist


    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function ShowUserlist(){
        session_start();

    if (!isset($_SESSION['user'])) {
        
        return redirect('login')->withErrors('You are not logged in- oops. Please log in.');}
        else {
        $users = \App\Http\Models\Appuser::all();
        return view('allview', compact('users')); }

       
    }
    public function ShowLogIn(){
if (isset($_SESSION['user'])) {
    return redirect('allusers')->withErrors('You are already logged in.'); }
     // show the form
    else {
    return view('login');
}

        
    }
    public function doLogOut(Request $request)
    {   session_start();
        if(!isset($_SESSION['user'])) {
        
        return redirect('login')->withErrors('You were not logged in- oops.');}
        else {
        session_unset(); 
        session_destroy(); 
        return redirect('login')->withErrors('Logged out successfully.');
        }
    }
    public function doLogin(Request $request)
   {
     // process the form
    // validate the info, create rules for the inputs
    
     // password can only be alphanumeric and has to be greater than 6 characters
    // run the validation rules on the inputs from the form
    $rules = array(
        'username'    => 'required', 
    'password' => 'required|alphaNum|min:6' );
    $validator = \Validator::make(Input::all(), $rules);
        
    

    // if the validator fails, redirect back to the form
      if ($validator->fails())
    {
   return redirect('login')->withErrors($validator); // send back all errors to the login form
      
       }
      else
        {
    // create our user data for the authentication
    $userdata = array(
        'username'     => $request->get('username'),
        'password'  => $request->get('password')
    );

    // attempt to do the login
    if (\Auth::attempt($userdata)) {
        session_start();
        extract($_POST);
         $_SESSION['user'] = $username;

         $getUsertype = DB::table('user')->where('username', $username)->first();
         $getUserArray = json_decode(json_encode($getUsertype), True);
         $_SESSION['usertype'] = $getUserArray['usertype_id'];
        return redirect('allusers');
        // validation successful
        // redirect them to the secure section or whatever
         
        // for now we'll just echo success (even though echoing in a controller is bad)
         


    } else {        
        return redirect('login')->withErrors('Username or password not found.');
        // validation not successful, send back to form 
        

    }

    }
    }

    public function editUsers($id) {
session_start();
    if (!isset($_SESSION['user'])) {
        
        return redirect('login')->withErrors('You are not logged in- oops. Please log in.');}
        elseif ($_SESSION['usertype'] == '2') {
        return view('notauthorised');
        }
        elseif ($_SESSION['usertype'] == '1') {
        
   $sql  = 'SELECT id, firstname, lastname, username, usertype_id, email, telephone ';
  $sql .= '  FROM user WHERE id = ?';
  $sql_params = array($id);
  $that = DB::table('user')->where('id', $id)->first();
  $form = json_decode(json_encode($that), True);
  }
        else {
         return view('notauthorised');
        }
  //return redirect('login')->withErrors($id);
  //$getUserArray = json_decode(json_encode($getUsertype), True);

  $data = array('title' => 'Edit Page', 'form' => $form);
  return view('edit')->with('data', $data);
    }
public function saveChanges($id, Request $request) {
    session_start();
    $firstname = $request->post('firstname');
    DB::update('update user set firstname = ? where id = ?', array($firstname, $id));
    $lastname = $request->post('lastname');
    DB::update('update user set lastname = ? where id = ?', array($lastname, $id));
    $username = $request->post('username');
    DB::update('update user set username = ? where id = ?', array($username, $id));
    $email = $request->post('email');
    DB::update('update user set email = ? where id = ?', array($email, $id));
    $telephone = $request->post('telephone');
    DB::update('update user set telephone = ? where id = ?', array($telephone, $id));
    $title = $request->post('title');
    DB::update('update user set title = ? where id = ?', array($title, $id));
    Session::forget('editor');
    return redirect('/allusers'); 
}

public function saveChanges_backup($id, Request $request) {
    
    $firstname = $request->get('firstname');
    DB::update('update user set firstname = ? where id = ?', array($firstname, $id));
    $lastname = $request->get('lastname');
    DB::update('update user set lastname = ? where id = ?', array($lastname, $id));
    $username = $request->get('username');
    DB::update('update user set username = ? where id = ?', array($username, $id));
    $email = $request->get('email');
    DB::update('update user set email = ? where id = ?', array($email, $id));
    $telephone = $request->get('telephone');
    DB::update('update user set telephone = ? where id = ?', array($telephone, $id));
    $title = $request->get('title');
    DB::update('update user set title = ? where id = ?', array($title, $id));
    Session::forget('editor');
    return redirect('/allusers');
}
}


