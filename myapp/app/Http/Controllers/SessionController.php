<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SessionController extends Controller {
   public function accessSessionData(Request $request){
      if($request->session()->has('my_name'))
         echo $request->session()->get('my_name');
      else
         echo 'No data in the session';
   }
   public function storeSessionData(Request $request){
      $request->session()->put('my_name','default');
      echo "Data has been added to session";
   }
   public function deleteSessionData(Request $request){
      $request->session()->forget('my_name');
      echo "Data has been removed from session.";
   }
}
/*///<?php //Session::put('firstname','firstname test');
        echo Session::get('firstname');
        //Session::put('firstname22','firstname test 22');
      if (Session::get('firstname22')) {
        echo Session::get('firstname22');
      }

      if (Session::get('firstname3')) {
        echo Session::get('firstname3');
      } else { echo ' 3 does not exist'; }

         ?>