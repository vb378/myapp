<?php


namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Contracts\Auth\Authenticatable as Authentic;


class Appuser extends Model implements Authentic
{
 use AuthenticatableTrait;

    protected $fillable = [
        'title', 'firstname','lastname',
        'email','telephone','password', 'username','usertype_id',
      
    ];
 	
    protected $table = 'user';
}

/*class Usertype extends Model
{
    protected $fillable = [
        'firstname','lastname',
    ];

    
    protected $table = 'userdetails';
}*/