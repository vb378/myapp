<!doctype html>
<?php //session_start() ?>

    <html lang="{{ app()->getLocale() }}">
    <head>
        <title>ALL USERS</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <!-- Styles etc. -->
    </head>
    <body>
    <div class="mx-auto" >
    <h1>Viewing all users...</h1>
    <table class="table table-hover">

    <thead>

      <th>Username</th>

      <th>User Type</th>

      <th>First name</th>

      <th>Last name</th>

      <th>User ID</th>

    </thead>

    <tbody>
@foreach($users as $user)



        <tr>

          <td>{{$user->username}} </td>

          <td><?php if ($user->usertype_id==1) {echo 'Administrator';}
          else {echo 'Normal User';} ?> </td>

          <td>{{$user->firstname}} </td>

          <td>{{$user->lastname}} </td>

          <td><a href={{ url('edit/'.$user->id) }}> <?php echo $user->id;
          ?> </a></td>




        </tr>
@endforeach
    </tbody>

</table>

    </div>
 <?php echo $_SESSION['user'];
 echo '
 ';

    echo "Login Success";

    echo "<a href='/logout'> Log out </a> "; ?> <p>

    <a href='admin'>Click here to quick add admins.</a>
    <p><a href='/'>Click here to add users.</a>
      
    </body>
    </html>