<html>

 <html lang="{{ app()->getLocale() }}">
 <head>
        <title>edit users</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <!-- Styles etc. -->
    </head>
    
<body><center>

<?php $form = $data['form'];
$id = $form['id'];
$username = $form['username']; ?>
<h1>Editing user {{$username}}:</h1>

<div class="mx-auto"  >
<form action={{('/save/'.$id) }} method="post" style=" display:inline!important;">
  Title:
  
    
    {{ 
       Form::select('title', ['Mr' => 'Mr', 'Ms' => 'Ms', 'Mrs' => 'Mrs', 'Dr' => 'Dr'], $form['title']) 
       }}
    
<p>
<div class='form-group'>
  First Name:
  <input type="text" name="firstname" value=<?php echo $form['firstname']; ?> />
  </div>
  </p>
  <p>
  <div class='form-group'>
  Last Name:
  <input type="text" name="lastname"  value=<?php echo $form['lastname']; ?> />
  </div>
  </p>
<p>
<div class='form-group'>
  Username:
  <input type="text" name="username" value=<?php echo $form['username']; ?> />
  </div>
  </p>
<p>
<div class='form-group'>
  E-mail: &nbsp &nbsp &nbsp
  <input type="text" name="email" value=<?php echo $form['email']; ?> />
  </div>
  </p>
<p>
<div class='form-group'>
  Telephone:
  <input type="text" name="telephone" value=<?php echo $form['telephone']; ?> />
  </div>
  </p>
<p>
<div class='form-group'>
  <button type='submit' class="btn btn-block btn">Save</button>
  <input type="hidden" name="id" value="<?php $form['id']; ?>" />
  </div>
  </p>
  <?php $_SESSION['editor']= $form['id'];
     ?>
</form>
</div>
</center>
</body>
</html>