<!doctype html>
<?php session_start() ?>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>User registration</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
	 <body>
   <center>
        If you do not have an account, please register below.
        Click <a href="/allusers">here</a> to view all users.
        Click<a href='/login'> here</a> if you already have an account.
        </center>
&nbsp
<center>
<h1>Registration</h1>
   </center>
    		<div class="mx-auto" style="width: 700px;" >
    		<form action='/users' method='get'>
     	 <div class='form-group'>
        <label for='title'>Title</label>
       {{
       Form::select('title', ['Mr' => 'Mr', 'Ms' => 'Ms', 'Mrs' => 'Mrs', 'Dr' => 'Dr'], 'Ms') }}
      </div>
      <div class='form-group'>
        <label for='firstname'>First Name</label>
        <input type='text' class="form-control" id='firstname' name='firstname' required>
      </div>
      <div class='form-group'>
        <label for='lastname'>Last Name</label>
        <input type='text' class="form-control" id='lastname' name='lastname' required>
      </div>
       <div class='form-group'>
        <label for='username'>Username</label>
        <input type='text' class="form-control" id='username' name='username' required>
      </div>
      <div class='form-group'>
        <label for='usertype_id'>Privilege Level</label>
        <select name='usertype_id' id = "usertype_id">
               <option value = ""> </option>
               <option value = "2">Normal User</option>
               <option value = "1">Administrator</option>
             </select>
      <div class='form-group'>
        <label for='email'>Email</label>
        <input type='email' class="form-control" id='email' name='email' required>
      </div>
      <div class='form-group'>
        <label for='telephone'>Telephone Number</label>
        <input type='tel' class="form-control" id='telephone' name='telephone' required>
      </div>
      <div class='form-group'>
        <label for='password'>Password</label>
        <input type='password' class="form-control" id='password' name='password' required>
      </div>
      <div>
        <button type='submit' class="btn btn-success btn-block">Submit</button>
      </div>
      @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    </form>
    </div>
  </body>
  
</html> 
