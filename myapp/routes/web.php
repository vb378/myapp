<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});


/*{
Route::get('/users', 'UserController@index');
}
Route::post('/users', function () {
    return View::make('usersview');
}); */

 Route::get('/users', 'UserController@store');
 Route::get('/allusers', 'UserController@ShowUserlist'); 
 // route to show the login form
 Route::get('login', array('uses' => 'UserController@showLogin'));

 // route to process the form
 Route::post('login', array('uses' => 'UserController@doLogin'));
 Route::get('logout', 'UserController@doLogOut');
 Route::get('admin', 'UserController@showAdmin');
 Route::get('entered', 'UserController@processAdmin');
 Route::get('edit/{id}', 'UserController@editUsers');
 Route::post('save/{id}', 'UserController@saveChanges');

//Route::get('/userss', 'UserController@store'); 
Route::get('session/get','SessionController@accessSessionData');
Route::get('session/set','SessionController@storeSessionData');
Route::get('session/remove','SessionController@deleteSessionData');